package sample;

import com.speedment.runtime.core.ApplicationBuilder;
import database.MydbApplication;
import database.MydbApplicationBuilder;
import database.MydbApplicationImpl;
import database._.schema.city.City;
import database._.schema.city.CityImpl;
import database._.schema.city.CityManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public Button btn;
    public ComboBox coBtn;

    public void doConn(ActionEvent actionEvent) {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MydbApplication mydbApplication= new MydbApplicationBuilder().withLogging(MydbApplicationBuilder.LogType.CONNECTION)
                .withLogging(MydbApplicationBuilder.LogType.STREAM).withLogging(MydbApplicationBuilder.LogType.PERSIST)
                .withLogging(MydbApplicationBuilder.LogType.UPDATE).build();
        CityManager cityManager = mydbApplication.getOrThrow(CityManager.class);
        cityManager.stream().forEach(System.out::println);

        City city = new CityImpl().setCityName("苗栗").setRowid(cityManager.stream().count()+1);
        cityManager.persist(city);

        List<String> citys = new ArrayList<>();
        cityManager.stream().forEach(e->{
                    if (e.getCityName().isPresent()){
                        citys.add(e.getCityName().get());
                    }
                }

        );

        ObservableList<String> observableList= FXCollections.observableList(citys);
        coBtn.setItems(observableList);
    }
}
