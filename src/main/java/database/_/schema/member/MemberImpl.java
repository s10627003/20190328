package database._.schema.member;

import database._.schema.member.generated.GeneratedMemberImpl;

/**
 * The default implementation of the {@link
 * database._.schema.member.Member}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class MemberImpl 
extends GeneratedMemberImpl 
implements Member {}